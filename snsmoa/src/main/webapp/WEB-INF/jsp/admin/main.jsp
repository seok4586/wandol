<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<h2> 
	반갑습니다. : ${pageContext.request.userPrincipal.name} | <a href="javascript:formSubmit()"> Logout</a>
</h2>
관리자 페이지 입니다.<br>


<p>  ${pageContext.request.userPrincipal.name} 님의 페이스북 담벼락 내용</p>

<table style="border:1px solid">
	<c:forEach var="list" items="${facebookList}" varStatus="loop">
	<c:forEach var="item" items="${list.data }" varStatus="loop">
	<tr>
		<td>${item.message }</td>
	</tr>
	</c:forEach>
	</c:forEach>
</table>

<%-- <c:url value="/logout" var="logoutUrl" /> --%>
<c:url value="/j_spring_security_logout" var="logoutUrl" />
<!-- csrt support -->
<form action="${logoutUrl}" method="post" id="logoutForm">
	<input type="hidden" 
		name="${_csrf.parameterName}"
		value="${_csrf.token}" />
</form>

<script>
	function formSubmit() {
		document.getElementById("logoutForm").submit();
	}
</script>
</body>
</html>