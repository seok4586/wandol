<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="oauthUrl" value="${oauthUrl}"></c:set>
    <title>facebook 로그인 하기 Sample</title>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script type="text/javascript">
<c:if test="${!empty oauthUrl and empty sessionScope.ACCESS_TOKEN}">
    window.open('${oauthUrl}',"facebookLogin", "width=1000, height=500, scrollbars=yes, resizable=yes");
</c:if>
</script>
<h1>
  Spring Social Facebook oAuth
</h1>
<table border="0">
    <tbody><tr>
        <td colspan="2">
            <input name="join" type="button" value="회원가입"> 
            <a href="${pageContext.request.contextPath}/facebook/facebookSignIn"><img src="http://www.koreanguide.co.kr/images/top_facebook.gif" align="absmiddle"></a>
        </td>
    </tr>
</tbody></table>
<form name="LoginForm" id="LoginForm" action="${pageContext.request.contextPath}/facebook/index" method="POST">
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>