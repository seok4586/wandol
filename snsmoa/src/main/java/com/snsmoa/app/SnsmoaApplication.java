package com.snsmoa.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.view.InternalResourceViewResolver;


@PropertySource("facebook.properties")
@SpringBootApplication
public class SnsmoaApplication {

	/**
	 * Spring Boot 실행 클래
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(SnsmoaApplication.class, args);
	}
	
	/**
	 * JSP View Resolver
	 * 표준 JSP 뷰 리졸
	 * @return InternalResourceViewResolver
	 */
	@Bean
    public InternalResourceViewResolver setupViewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/jsp/");
        resolver.setSuffix(".jsp");
        return resolver;
    }
}
