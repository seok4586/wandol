package com.snsmoa.app.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mongodb.DBObject;
import com.snsmoa.app.dao.FacebookDao;
import com.snsmoa.app.service.FacebookService;

@Service("facebookService")
public class FacebookServiceImpl implements FacebookService {

	@Autowired
	FacebookDao facebookDao;
	
	@Override
	public List<?> getFacebookList() {
		// TODO Auto-generated method stub
		return facebookDao.getFacebookData();
	}

}
