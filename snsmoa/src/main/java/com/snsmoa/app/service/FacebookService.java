package com.snsmoa.app.service;

import java.util.List;

import com.mongodb.DBObject;

/**
 * FaceBook Service Class
 * @author hyungeun.jung
 * @date 2015.01.01
 */
public interface FacebookService {
	
	public List<?> getFacebookList();

}
