package com.snsmoa.app.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.snsmoa.app.service.FacebookService;

/**
 * Main Controller
 * @author hyungeun.jung
 * @date 2015.01.01
 */
@Controller
public class MainController {
	
	@Autowired
	FacebookService facebookService;

	/**
	 * 홈으로 이동
	 * @return
	 */
	@RequestMapping("/")
	public String main(){
		return "main/home";
	}
	
	/**
	 * 관리자 페이지 이동 
	 * @return
	 */
	@RequestMapping("/admin")
	public String admin(Model model){
		model.addAttribute("facebookList", facebookService.getFacebookList());
		return "admin/main";
	}

}
