package com.snsmoa.app.web;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.User;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.snsmoa.app.service.FacebookService;

/**
 * FaceBook Controller
 * @author hyungeun.jung
 * @date 2015.01.02
 */
@Controller
@RequestMapping("/facebook")
@PropertySource("classpath:/facebook.properties")

public class FaceBookController {

	private static final Logger logger = LoggerFactory.getLogger(FaceBookController.class);


	@Autowired
	FacebookService facebookService;
	@Autowired
	private Environment environment;

	@RequestMapping(value = "/index", method = RequestMethod.POST)
	public String index(HttpSession session, Model model){
		//facebook info setting
		Facebook facebook = new FacebookTemplate((String)session.getAttribute("ACCESS_TOKEN"));
		User profile = facebook.userOperations().getUserProfile();
		List<String> friends = facebook.friendOperations().getFriendIds();
		//byte[] profiles = facebook.userOperations().getUserProfileImage();
		String myId = profile.getId();
		String myName = profile.getName();
		String myEmail = profile.getEmail();
		String myGender = profile.getGender();
		for(String s : friends){
			logger.info("--" + s);
		}
		Map<String,Object> profileInfo = new HashMap<String, Object>();
		profileInfo.put("myId", myId);
		profileInfo.put("myName", myName);
		profileInfo.put("myEmail", myEmail);
		profileInfo.put("myGender", myGender);
		profileInfo.put("myFriends", friends);
		logger.info("myId=>{}", myId);
		logger.info(">>>>> profile => {}",profile.getName());
		
		model.addAttribute("profileInfo", profileInfo);

		return "facebook/index";
	}


	/**
	 * facebookSignin
	 * 페이스북 로그인 팝업 호출
	 * @param response
	 * @param request
	 * @param model
	 * @return
	 * @scope user_about_me:사용자 본인 정보, publish_stream: 기본적인 쓰기권한, read_friendlists: 친구 목록      
	 * @throws Exception
	 */
	@RequestMapping(value="/facebookSignIn" , method = RequestMethod.GET)
	public String facebookSignin(HttpServletResponse response, HttpServletRequest request, Model model) throws Exception {
		logger.info("{}","facebookSignIn START");
		String callbackUrl = environment.getProperty("callback.host")+"/facebook/facebookAccessToken";
		String oauthUrl = "https://www.facebook.com/dialog/oauth?" +
				"client_id="+ environment.getProperty("facebook.appId")+"&redirect_uri="+URLEncoder.encode(callbackUrl, "UTF-8")+
				//"&scope=user_about_me,publish_stream,read_friendlists,offline_access";
				"&scope=email,public_profile,user_friends";
		logger.info("callbackUrl:" + callbackUrl);
		logger.info("FACEBOOK URL:" + oauthUrl);
		model.addAttribute("oauthUrl", oauthUrl);
		return "facebook/home";
	}

	/**
	 * facebookAccessToken
	 * 페이스북 로그인 결과 리턴
	 * @param request
	 * @param model
	 * @param message
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	@RequestMapping(value="/facebookAccessToken")
	public String getFacebookClientAccessToken(HttpServletRequest request, Model model, @RequestParam(value="message", defaultValue="" , required=false) String message, HttpServletResponse response) throws Exception {
		String code             = request.getParameter("code");
		String errorReason      = request.getParameter("error_reason");
		String errorDescription = request.getParameter("error_description");

		if(logger.isInfoEnabled()){
			logger.info("code => " +code);
			logger.info("errorReason code => " + errorReason);
			logger.info("errorDescription => " + errorDescription);
		}
		//facebook accessToken call
		requestAccesToken(request, code);

		return "facebook/callback";
	}

	/**
	 * facebook accessToken request
	 * 토큰값 받아옴
	 * @param request
	 * @param code
	 * @throws Exception
	 */
	public void requestAccesToken(HttpServletRequest request, String code) throws Exception{
		String accessToken = "";
		String result      = "";

		String callbackUrl = environment.getProperty("callback.host")+"/facebook/facebookAccessToken";
		String url = "https://graph.facebook.com/oauth/access_token"+
				"?client_id="+environment.getProperty("facebook.appId")+
				"&client_secret="+environment.getProperty("facebook.secretKey")+
				"&redirect_uri="+URLEncoder.encode(callbackUrl, "UTF-8")+
				"&code="+code;

		//httpGet 통신
		HttpGet httpGet = new HttpGet(url);
		DefaultHttpClient httpClient = new DefaultHttpClient();
		result = httpClient.execute(httpGet, new BasicResponseHandler());
		accessToken = result.split("&")[0].replaceFirst("access_token=", "");

		//토큰값 세션처리
		setAccessToken(request, accessToken);
	}

	/**
	 * setAccessToken
	 * 토큰값 세션 저장
	 * @param request
	 * @param accessToken
	 * @throws Exception
	 */
	public void setAccessToken(HttpServletRequest request, String accessToken) throws Exception {
		request.getSession().setAttribute("ACCESS_TOKEN", accessToken);
		logger.info("ACCESS_TOKEN => " + request.getSession().getAttribute("ACCESS_TOKEN"));
	}




	/**
	 * 페이스북 로그인  페이지 이동 
	 * @return
	 */
	@RequestMapping(value="/home", method=RequestMethod.GET)
	public String login() {
		return "facebook/home";
	}

}
