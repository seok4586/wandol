package com.snsmoa.app.social;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.social.UserIdSource;
import org.springframework.social.config.annotation.ConnectionFactoryConfigurer;
import org.springframework.social.config.annotation.EnableSocial;
import org.springframework.social.config.annotation.SocialConfigurerAdapter;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.mem.InMemoryUsersConnectionRepository;
import org.springframework.social.connect.web.SignInAdapter;
import org.springframework.web.context.request.NativeWebRequest;

@Configuration
@EnableSocial
public class SocialConfig extends SocialConfigurerAdapter {

	@Override
	public void addConnectionFactories(ConnectionFactoryConfigurer connectionFactoryConfigurer,
			Environment environment) {
		// TODO Auto-generated method stub
		super.addConnectionFactories(connectionFactoryConfigurer, environment);
	}

	@Override
	public UserIdSource getUserIdSource() {
		// TODO Auto-generated method stub
		return super.getUserIdSource();
	}

	@Override
    public UsersConnectionRepository getUsersConnectionRepository(ConnectionFactoryLocator connectionFactoryLocator) {
        InMemoryUsersConnectionRepository repository = new InMemoryUsersConnectionRepository(connectionFactoryLocator);
        repository.setConnectionSignUp(new QuickConnectionSignup());
        return repository;
    }
	
	@Bean
    public SignInAdapter signInAdapter() {
        return new QuickSignInAdapter(new HttpSessionRequestCache());
    }
	
	private static class QuickConnectionSignup implements ConnectionSignUp {

		@Override
		public String execute(Connection<?> connection) {
			// TODO Auto-generated method stub
			return connection.getKey().getProviderUserId();
		}
        
    }
	
	public class QuickSignInAdapter implements SignInAdapter {
	    private final RequestCache requestCache;
	    @Autowired
	    public QuickSignInAdapter(RequestCache requestCache) {
	        this.requestCache = requestCache;
	    }
	    @Override
	    public String signIn(String localUserId, Connection<?> connection, NativeWebRequest request) {
	        String providerUserId = connection.getKey().getProviderUserId();
	        signin(providerUserId);
	        return extractOriginalUrl(request);
	    }
	    private String extractOriginalUrl(NativeWebRequest request) {
	        return "";
	    }
	    private void removeAutheticationAttributes(HttpSession session) {
	        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
	    }
	    private void signin(String userId) {
	        SecurityContextHolder.getContext().setAuthentication(
	                new UsernamePasswordAuthenticationToken(userId, null, null));
	    }
	}
	
	
	
	

}
