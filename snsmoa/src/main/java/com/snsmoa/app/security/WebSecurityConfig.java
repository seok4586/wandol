package com.snsmoa.app.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Spring Security Web Configure
 * 시큐리티 설정 클래스
 * 2016.01.01
 * @author hyungeunjung
 *
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	
	/**
	 * Http 기반 공통 설정 
	 */
	protected void configure(HttpSecurity http) throws Exception{
		
		CustomLogoutHandler customLogoutHandler = new CustomLogoutHandler();
		http
        .authorizeRequests()
        	.antMatchers("/login/**").permitAll()		
            .antMatchers("/resources/**").permitAll()  
            .anyRequest().authenticated()
            .antMatchers("/admin/**").hasRole("ADMIN")
            .and()
        .formLogin()
            .loginPage("/login")
            .failureUrl("/login?error")
            .usernameParameter("userId")
            .passwordParameter("userPw")
            .permitAll()
            .and()
        .logout()
        	.logoutUrl("/j_spring_security_logout")
        	.logoutSuccessUrl("/login?logout")
            .permitAll();
	}
	
	/**
	  인메모리 유저 인증 
	 * @param auth
	 * @throws Exception
	 */
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        
    	auth
    		.inMemoryAuthentication()
                .withUser("antkdi")
                .password("1234")
                .roles("ADMIN")
            .and()
        		.withUser("wanseok.park")
        		.password("1234")
        		.roles("ADMIN")
	    	.and()
				.withUser("ungmani")
				.password("1234")
				.roles("ADMIN"); 
    }
}