package com.snsmoa.app.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.mongodb.DBObject;

@Repository
public class FacebookDao {
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	public List<?> getFacebookData(){
		Query query = new Query();
		return mongoTemplate.find(query, DBObject.class,"FACEBOOK");
		
	}

}
